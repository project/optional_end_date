# Optional end date

Make the end date in a Date range field (DateRangeItem) optional.

An extra "Optional end date" checkbox is added to the Date range field type
Storage settings. When the box is checked, the end date is no longer required.

This will mimic the behavior of optional end dates in Drupal 8.9.x. So when
8.9.x is ready, you should be able to uninstall this module after updating, and
use the core implementation instead.

See [Allow end date to be optional](https://www.drupal.org/project/drupal/issues/2794481)
for more information about the core implementation.

For a full description of the module, visit the [project page](https://www.drupal.org/project/optional_end_date).

To submit bug reports and feature suggestions, or to track changes, use the [issue queue](https://www.drupal.org/project/issues/optional_end_date).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

You need to check the "Optional end date" setting for all existing and new
daterange fields, where the end date is not required.
This is done in the "Storage settings" form of the daterange fields.


## Maintainers

- Birk - [Birk](https://www.drupal.org/u/birk)
